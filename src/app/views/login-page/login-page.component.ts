import { Component } from '@angular/core';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {

  title = 'Your secured decentralized storage using blockchain for your certificates.';
  form_title = 'Log In';
  email= ""; 
  password= "";

  login() {
    console.log(this.email);
  }
}
