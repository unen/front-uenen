import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front';

  navList = [
    {
      route: "about",
      name: "About",
      color: null,
    },
    {
      route: "technology",
      name: "Technology",
      color: null,
    },
    {
      route: "learn",
      name: "Learn",
      color: null,
    },
    {
      route: "login",
      name: "Sign In",
      color: "signin",
    }
  ];

  constructor(
    private router: Router,
  ) {} 

  redirectTo(route: string) {
    console.log(route);
    this.router.navigateByUrl(route);
  }
}
